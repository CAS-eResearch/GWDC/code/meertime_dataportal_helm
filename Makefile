.PHONY: lint package values render

lint:
	helm lint

package: lint
	rm -rf *.tgz
	@$(call f_helm,package $(PWD))

upload: package
	curl -u "$(_HELM_USR):$(_HELM_PSW)" https://nexus.gwdc.org.au/repository/helm/ --upload-file `ls *.tgz` -v

values:
	$(call f_helm,show values .)

render:
	@$(call f_helm,template .)

_OCI_HELM_IMAGE=alpine/helm
_OCI_HELM_TAG=latest
_OCI_HELM_MOUNT=/mnt/src
define f_helm
	docker run --rm -ti -v $(PWD):$(_OCI_HELM_MOUNT) -w $(_OCI_HELM_MOUNT) $(_OCI_HELM_IMAGE):$(_OCI_HELM_TAG) $(1)
endef